package main

import (
	"encoding/json"
	"fmt"
	"log"
	rand "math/rand"
	"os"
	"strconv"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

var (
	brokerHost           = getEnvStr("SENSOR_BROKER_HOST", "localhost")
	brokerPort           = getEnvInt("SENSOR_BROKER_PORT", 1883)
	brokerUsername       = getEnvStr("SENSOR_BROKER_USERNAME", "admin")
	brokerPassword       = getEnvStr("SENSOR_BROKER_PASSWORD", "admin")
	mqttTopic            = getEnvStr("SENSOR_MQTT_TOPIC", "")
	mqttQos              = intToByte(getEnvInt("SENSOR_MQTT_QOS", 1))
	publishPeriod        = getEnvInt("SENSOR_PUBLISH_PERIOD", 1000)
	sensorDeviceID       string
	sensorDeviceLocation = getEnvStr("SENSOR_LOCATION", "Region_A")

	temperatureConfig = measurementConfig{
		ValueRange:          [2]float64{getEnvFloat64("SENSOR_TEMP_RANGE_MIN", -100.0), getEnvFloat64("SENSOR_TEMP_RANGE_MAX", 100.0)},
		CurrentValue:        getEnvFloat64("SENSOR_TEMP_INIT_VALUE", 0.0),
		ValueIncrementRange: [2]float64{getEnvFloat64("SENSOR_TEMP_INCREMENT_MIN", -10.0), getEnvFloat64("SENSOR_TEMP_INCREMENT_MAX", 10.0)},
	}
	pressureConfig = measurementConfig{
		ValueRange:          [2]float64{getEnvFloat64("SENSOR_PRESSURE_RANGE_MIN", 950.0), getEnvFloat64("SENSOR_PRESSURE_RANGE_MAX", 1084.0)},
		CurrentValue:        getEnvFloat64("SENSOR_PRESSURE_INIT_VALUE", 1013.25),
		ValueIncrementRange: [2]float64{getEnvFloat64("SENSOR_PRESSURE_INCREMENT_MIN", -1.5), getEnvFloat64("SENSOR_PRESSURE_INCREMENT_MAX", 1.5)},
	}
	humidityConfig = measurementConfig{
		ValueRange:          [2]float64{getEnvFloat64("SENSOR_HUMIDITY_RANGE_MIN", 0.0), getEnvFloat64("SENSOR_HUMIDITY_RANGE_MAX", 100.0)},
		CurrentValue:        getEnvFloat64("SENSOR_HUMIDITY_INIT_VALUE", 60.0),
		ValueIncrementRange: [2]float64{getEnvFloat64("SENSOR_HUMIDITY_INCREMENT_MIN", -2.75), getEnvFloat64("SENSOR_HUMIDITY_INCREMENT_MAX", 2.75)},
	}
	batteryConfig = measurementConfig{
		ValueRange:          [2]float64{0.0, 100.0},
		CurrentValue:        100.0,
		ValueIncrementRange: [2]float64{-getEnvFloat64("SENSOR_BATTERY_DECREMENT", 0.0045), 0.0},
	}
	stopOnLowBattery = getEnvInt("SENSOR_STOP_ON_LOW_BATTERY", 0)
)

type measurementConfig struct {
	ValueRange          [2]float64
	CurrentValue        float64
	ValueIncrementRange [2]float64
}

type payload struct {
	Measurement  measurement  `json:"measurement"`
	DeviceStatus deviceStatus `json:"deviceStatus"`
}

type measurement struct {
	DateTime    string  `json:"datetime"`
	Temperature float64 `json:"temperature"`
	Pressure    float64 `json:"pressure"`
	Humidity    float64 `json:"humidity"`
}

type deviceStatus struct {
	DeviceID       string  `json:"deviceId"`
	DeviceLocation string  `json:"deviceLocation"`
	BatteryLevel   float64 `json:"batteryLevel"`
}

func main() {
	log.Print("Started")
	initVars()

	if arg := os.Args[1:]; len(arg) >= 1 && arg[0] == "debug" {
		count := 1
		if len(arg) >= 2 {
			count, _ = strconv.Atoi(arg[1])
		}
		for i := 1; i <= count; i++ {
			payload, _ := json.MarshalIndent(generatePayload(), "", "  ")
			log.Printf("Topic: %s\nPayload: %s", mqttTopic, payload)
		}
		os.Exit(0)
	}

	client := buildConnectedClient(fmt.Sprintf("%d", time.Now().UnixNano()))
	if publishPeriod <= 0 {
		for {
			payload, _ := json.Marshal(generatePayload())
			client.Publish(mqttTopic, mqttQos, false, payload)
			// token.Wait()
			log.Printf("Published: %s", payload)
		}
	} else {
		timer := time.NewTicker(time.Duration(publishPeriod) * time.Millisecond)
		for range timer.C {
			payload, _ := json.Marshal(generatePayload())
			client.Publish(mqttTopic, mqttQos, false, payload)
			// token.Wait()
			log.Printf("Published: %s", payload)
		}
	}

	log.Println("Exit:", 0)
}

func getEnvStr(key string, defaultValue string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return defaultValue
}

func getEnvInt(key string, defaultValue int) int {
	if value, ok := os.LookupEnv(key); ok {
		if numberValue, err := strconv.Atoi(value); err != nil {
			log.Fatalln("Failed to parse", value, "as integer for", key)
		} else {
			return numberValue
		}
	}
	return defaultValue
}

func getEnvFloat64(key string, defaultValue float64) float64 {
	if value, ok := os.LookupEnv(key); ok {
		if numberValue, err := strconv.ParseFloat(value, 64); err != nil {
			log.Fatalln("Failed to parse", value, "as float for", key)
		} else {
			return numberValue
		}
	}
	return defaultValue
}

func intToByte(value int) byte {
	switch value {
	case 0:
		return 0
	case 1:
		return 1
	case 2:
		return 2
	default:
		return 0
	}
}

func initVars() {
	hostname, _ := os.Hostname()
	sensorDeviceID = hostname
	rand.Seed(time.Now().UnixNano())

	if mqttTopic == "" {
		mqttTopic = fmt.Sprintf("mqtt_device/%s/readings/%s", sensorDeviceID, sensorDeviceLocation)
	}
}

func buildConnectedClient(instanceID string) mqtt.Client {
	// Client
	opts := mqtt.NewClientOptions()
	opts.AddBroker(fmt.Sprintf("tcp://%s:%d", brokerHost, brokerPort))
	opts.SetUsername(brokerUsername)
	opts.SetPassword(brokerPassword)
	opts.SetClientID(instanceID)
	client := mqtt.NewClient(opts)

	// Connect
	token := client.Connect()
	for !token.WaitTimeout(3 * time.Second) {
	}
	if err := token.Error(); err != nil {
		log.Fatal(err)
	} else {
		log.Print("Connected")
	}

	return client
}

func generatePayload() payload {
	payload := payload{
		Measurement: measurement{
			DateTime:    fmt.Sprint(int64(time.Nanosecond) * time.Now().UnixNano() / int64(time.Millisecond)),
			Temperature: getReading(&temperatureConfig),
			Pressure:    getReading(&pressureConfig),
			Humidity:    getReading(&humidityConfig),
		},
		DeviceStatus: deviceStatus{
			DeviceID:       sensorDeviceID,
			DeviceLocation: sensorDeviceLocation,
			BatteryLevel:   getBatteryReading(&batteryConfig),
		},
	}
	return payload
}

func getReading(config *measurementConfig) float64 {
	increment := config.ValueIncrementRange[0] + rand.Float64()*(config.ValueIncrementRange[1]-config.ValueIncrementRange[0])
	newValue := config.CurrentValue + increment
	if newValue < config.ValueRange[0] || newValue > config.ValueRange[1] {
		newValue = config.CurrentValue - increment
	}
	config.CurrentValue = newValue
	return config.CurrentValue
}

func getBatteryReading(batteryConfig *measurementConfig) float64 {
	if batteryConfig.CurrentValue <= 0 {
		return 0
	}

	increment := batteryConfig.ValueIncrementRange[0] + rand.Float64()*(batteryConfig.ValueIncrementRange[1]-batteryConfig.ValueIncrementRange[0])
	newValue := batteryConfig.CurrentValue + increment
	if newValue <= 0 && stopOnLowBattery == 1 {
		log.Fatal("Out of juice")
	}
	batteryConfig.CurrentValue = newValue
	return batteryConfig.CurrentValue
}

func randFloats(min, max float64, n int) []float64 {
	res := make([]float64, n)
	for i := range res {
		res[i] = min + rand.Float64()*(max-min)
	}
	return res
}
