# IoT sensors monitoring platform

IoT sensors monitoring. It is based on RabbitMQ, Telegraf and VictoriaMetrics stack.

|         |                 | 
| ---     | ---             | 
| Faculty | ELFAK           |
| Student | Dalibor Aleksic |
| Index   | 888             |


## Overview

> TODO

## Deployment

This PoC uses Hetzner CPX31 instance as host. Can be deployed via 2 variants:
- Docker variant (docker-compose)
- Kubernetes variant

### Docker variant

#### Requirements

- docker
- docker-compose

#### Installation
##### Install requirements
```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update -y
apt-get install -y docker-ce docker-ce-cli containerd.io
systemctl start docker && systemctl enable docker
curl -L "https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```
##### Start PoC
```bash
./run_on_docker.sh
```
##### Stop PoC & remove resources
```bash
./stop_on_docker.sh
```

### Kubernetes variant
#### Requirements

- docker
- microk8s

#### Installation
##### Install requsirements
```bash
apt install snapd
systemctl enable snapd && systemctl start snapd
snap install microk8s --classic --channel 1.19/stable
microk8s start
```
##### Start PoC
```bash
./run_on_kubernetes.sh
```
##### Stop PoC & remove resources
```bash
./stop_on_kubernetes.sh
```

## Usage

Services:

| Port | Service | Info | Example URL |
|---   |--- |--- |--- |
|32672 | RabbitMQ | RabbitMQ Dashboard | http://<HOST_IP>:32672 |
|30080 | Grafana  | Grafana Dashboard  | http://<HOST_IP>:30080 |
