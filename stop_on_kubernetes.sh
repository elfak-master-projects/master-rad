#!/usr/bin/env bash
set -e
cd "$(dirname "${BASH_SOURCE[0]}")"

echo "==> Remove k8s"
microk8s kubectl delete -k k8s/

echo "==> DONE"