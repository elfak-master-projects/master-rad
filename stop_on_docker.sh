#!/usr/bin/env bash
set -e
cd "$(dirname "${BASH_SOURCE[0]}")"

echo "==> STOP"
docker-compose down --volumes

echo "==> DONE"