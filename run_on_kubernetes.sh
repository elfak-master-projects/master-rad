#!/usr/bin/env bash
set -e
cd "$(dirname "${BASH_SOURCE[0]}")"

echo "==> Build images"
docker build -t masterad/rabbitmq-mqtt rabbitmq/
docker build -t masterad/sensor-go sensor/golang/

echo "==> Publish images"
while read -r line; do
  IMG_NAME=$(echo $line | awk '{ print $1 }' | awk -F'/' '{ print $2}')
  IMG_ID=$(echo $line | awk '{ print $2 }')

  echo "-> Publish $IMG_NAME"
  docker tag $IMG_ID localhost:32000/$IMG_NAME:latest
  docker push localhost:32000/$IMG_NAME:latest
done < <(docker images | grep masterad | awk '{print $1,$3}')


echo "==> Apply k8s"
microk8s kubectl apply -k k8s/

echo "==> DONE"