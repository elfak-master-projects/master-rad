#!/usr/bin/env bash
set -e
cd "$(dirname "${BASH_SOURCE[0]}")"

echo "==> START"
docker-compose up --build -d

echo "==> DONE"